﻿using ChessRating;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace TestEloRating
{
    public class TestEloRating
    {
        public static IEnumerable<object[]> Data =>
            new List<object[]> {
                new object[] { 1000, 1000, 0.5m},
                new object[] { 1003, 1000, 0.5m},
                new object[] { 1000, 1003, 0.5m},
                new object[] { 1000, 1010, 0.49m},
                new object[] { 1000, 1017, 0.48m},
                new object[] { 1025, 1000, 0.53m},
                new object[] { 1000, 1032, 0.46m},
                new object[] { 1000, 1735, 0.01m},
                new object[] { 1736, 1000, 1m}
            };

        [Theory]
        [MemberData(nameof(Data))]
        public void TestExpectedScore(int ownRating, int opponentRating, decimal expectedResult)
        {
            var actual = EloRating.ExpectedScore(ownRating, opponentRating);
            Assert.Equal(expectedResult, actual, 3);
        }
    }
}
