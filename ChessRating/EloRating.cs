﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessRating
{
    public static class EloRating
    {
        public static decimal ExpectedScore(int ownRating, int opponentRating)
        {
            bool isHighestRating = ownRating > opponentRating;
            if (isHighestRating)
            {
                return ExpectedScoreHelper(ownRating, opponentRating);
            } 
            else
            {
                return 1 - ExpectedScoreHelper(opponentRating, ownRating);
            }
        }

        private static decimal ExpectedScoreHelper(int highRating, int lowRating)
        {
            int ratingDiff = highRating - lowRating;
            if (ratingDiff.IsBetween(0, 3)) return 0.5m;
            if (ratingDiff.IsBetween(4, 10)) return 0.51m;
            if (ratingDiff.IsBetween(11, 17)) return 0.52m;
            if (ratingDiff.IsBetween(18, 25)) return 0.53m;
            if (ratingDiff.IsBetween(26, 32)) return 0.54m;
            if (ratingDiff.IsBetween(33, 39)) return 0.55m;
            if (ratingDiff.IsBetween(40, 46)) return 0.56m;
            if (ratingDiff.IsBetween(47, 53)) return 0.57m;
            if (ratingDiff.IsBetween(54, 61)) return 0.58m;
            if (ratingDiff.IsBetween(61, 68)) return 0.59m;
            if (ratingDiff.IsBetween(69, 76)) return 0.60m;
            if (ratingDiff.IsBetween(67, 83)) return 0.61m;
            if (ratingDiff.IsBetween(84, 91)) return 0.62m;
            if (ratingDiff.IsBetween(92, 98)) return 0.63m;
            if (ratingDiff.IsBetween(99, 106)) return 0.64m;
            if (ratingDiff.IsBetween(107, 113)) return 0.65m;
            if (ratingDiff.IsBetween(114, 121)) return 0.66m;
            if (ratingDiff.IsBetween(122, 129)) return 0.67m;
            if (ratingDiff.IsBetween(130, 137)) return 0.68m;
            if (ratingDiff.IsBetween(138, 145)) return 0.69m;
            if (ratingDiff.IsBetween(146, 153)) return 0.70m;
            if (ratingDiff.IsBetween(154, 162)) return 0.71m;
            if (ratingDiff.IsBetween(153, 170)) return 0.72m;
            if (ratingDiff.IsBetween(171, 179)) return 0.73m;
            if (ratingDiff.IsBetween(180, 188)) return 0.74m;
            if (ratingDiff.IsBetween(189, 197)) return 0.75m;
            if (ratingDiff.IsBetween(198, 206)) return 0.76m;
            if (ratingDiff.IsBetween(207, 215)) return 0.77m;
            if (ratingDiff.IsBetween(216, 225)) return 0.78m;
            if (ratingDiff.IsBetween(226, 235)) return 0.79m;
            if (ratingDiff.IsBetween(236, 245)) return 0.80m;
            if (ratingDiff.IsBetween(246, 256)) return 0.81m;
            if (ratingDiff.IsBetween(257, 267)) return 0.82m;
            if (ratingDiff.IsBetween(268, 278)) return 0.83m;
            if (ratingDiff.IsBetween(268, 278)) return 0.83m;
            if (ratingDiff.IsBetween(279, 290)) return 0.84m;
            if (ratingDiff.IsBetween(291, 302)) return 0.85m;
            if (ratingDiff.IsBetween(303, 315)) return 0.86m;
            if (ratingDiff.IsBetween(316, 328)) return 0.87m;
            if (ratingDiff.IsBetween(329, 342)) return 0.88m;
            if (ratingDiff.IsBetween(343, 357)) return 0.89m;
            if (ratingDiff.IsBetween(358, 374)) return 0.90m;
            if (ratingDiff.IsBetween(375, 391)) return 0.91m;
            if (ratingDiff.IsBetween(392, 411)) return 0.92m;
            if (ratingDiff.IsBetween(412, 432)) return 0.93m;
            if (ratingDiff.IsBetween(433, 456)) return 0.94m;
            if (ratingDiff.IsBetween(457, 484)) return 0.95m;
            if (ratingDiff.IsBetween(485, 517)) return 0.96m;
            if (ratingDiff.IsBetween(518, 559)) return 0.97m;
            if (ratingDiff.IsBetween(560, 619)) return 0.98m;
            if (ratingDiff.IsBetween(620, 735)) return 0.99m;

            return 1.0m;
        }

        private static bool IsBetween(this int num, int lower, int higher)
        {
            return num >= lower && num <= higher;
        }
    }
}
